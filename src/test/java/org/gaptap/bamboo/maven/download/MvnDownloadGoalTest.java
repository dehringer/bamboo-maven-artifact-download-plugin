/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.maven.download;

import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.ARTIFACT_ID;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.CLASSIFIER;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.DOWNLOAD_STRATEGY;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.DOWNLOAD_STRATEGY_MANUAL;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.DOWNLOAD_STRATEGY_POM;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.GROUP_ID;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.POM_CLASSIFIER;
import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.*;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.davidehringer.bamboo.maven.extractor.PomValueExtractor;

/**
 * @author David Ehringer
 */
public class MvnDownloadGoalTest {

    private PomValueExtractor extractorPom;
    private CommonTaskContext taskContext;
    private File destination;

    @Before
    public void setup() {
        extractorPom = mock(PomValueExtractor.class);
        when(extractorPom.getValue("groupId")).thenReturn("org.mockito");
        when(extractorPom.getValue("artifactId")).thenReturn("mockito-all");
        when(extractorPom.getValue("version")).thenReturn("1.9.5");

        destination = new File("target");
        taskContext = mock(CommonTaskContext.class);
        when(taskContext.getWorkingDirectory()).thenReturn(destination);
    }

    @Test
    public void commandLineFromPomFileForBasicJarFile() {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_POM);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(
                copy.getCommandLine(),
                startsWith("org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=org.mockito:mockito-all:1.9.5:jar"));
    }

    @Test
    public void commandLineFromPomFileForJarFileWithClassifier() {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_POM);
        configurationMap.put(POM_CLASSIFIER, "javadoc");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(
                copy.getCommandLine(),
                startsWith("org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=org.mockito:mockito-all:1.9.5:jar:javadoc"));
    }

    @Test
    public void commandLineFromManualForBasicJarFile() {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_MANUAL);
        configurationMap.put(GROUP_ID, "org.hamcrest");
        configurationMap.put(ARTIFACT_ID, "hamcrest-all");
        configurationMap.put(PACKAGING, "jar");
        configurationMap.put(VERSION, "1.1");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(
                copy.getCommandLine(),
                startsWith("org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=org.hamcrest:hamcrest-all:1.1:jar"));
    }

    @Test
    public void commandLineFromManualForBasicWarFile() {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_MANUAL);
        configurationMap.put(GROUP_ID, "org.hamcrest");
        configurationMap.put(ARTIFACT_ID, "hamcrest-all");
        configurationMap.put(PACKAGING, "war");
        configurationMap.put(VERSION, "1.1");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(
                copy.getCommandLine(),
                startsWith("org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=org.hamcrest:hamcrest-all:1.1:war"));
    }

    @Test
    public void commandLineFromManualForJarFileWithClassifier() {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_MANUAL);
        configurationMap.put(GROUP_ID, "org.hamcrest");
        configurationMap.put(ARTIFACT_ID, "hamcrest-all");
        configurationMap.put(VERSION, "1.1");
        configurationMap.put(PACKAGING, "jar");
        configurationMap.put(CLASSIFIER, "sources");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(
                copy.getCommandLine(),
                startsWith("org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=org.hamcrest:hamcrest-all:1.1:jar:sources"));
    }

    @Test
    public void theCommandLineContainsTheDestinationDirectoryForTheFile() throws IOException {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_MANUAL);
        configurationMap.put(GROUP_ID, "org.hamcrest");
        configurationMap.put(ARTIFACT_ID, "hamcrest-all");
        configurationMap.put(VERSION, "1.1");
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(copy.getCommandLine(), containsString(" -DoutputDirectory=" + destination.getCanonicalPath()));
    }

    @Test
    public void theCommandLineContainsTheAdditionalCommandLineArguments() throws IOException {
        ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_MANUAL);
        configurationMap.put(GROUP_ID, "org.hamcrest");
        configurationMap.put(ARTIFACT_ID, "hamcrest-all");
        configurationMap.put(VERSION, "1.1");

        String additionalArgs = "-P myProfile";
        configurationMap.put(MAVEN_ADDITIONAL_ARGS, additionalArgs);
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        MvnDownloadGoal copy = new MvnDownloadGoal(taskContext, extractorPom);
        assertThat(copy.getCommandLine(), containsString(" " + additionalArgs));
    }
}
