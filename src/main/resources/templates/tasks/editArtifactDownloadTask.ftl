[#-- @ftlvariable name="uiConfigSupport" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[@ui.bambooSection titleKey='maven.artifact.download.artifact']

	[@ww.textfield labelKey='maven.artifact.download.destination' name='destination' cssClass="long-field" /]
	
	[@ww.select labelKey='maven.artifact.download.strategy' name='downloadStrategy'
	                                        listKey='key' listValue='value' toggle='true'
	                                        list=downloadStrategies /]
	
	[@ui.bambooSection dependsOn="downloadStrategy" showOn="0"]
		[@ww.textfield labelKey='maven.artifact.download.pom' name='pomFile' required='true' cssClass="long-field" /]
		[@ww.textfield labelKey='maven.artifact.download.pom.classifier' name='pomClassifier' required='false' /]
	[/@ui.bambooSection]
	
	[@ui.bambooSection dependsOn="downloadStrategy" showOn="1"]
		[@ww.textfield labelKey='maven.artifact.download.manual.groupId' name='groupId' required='true' cssClass="long-field" /]
		[@ww.textfield labelKey='maven.artifact.download.manual.artifactId' name='artifactId' required='true' /]
		[@ww.textfield labelKey='maven.artifact.download.manual.version' name='version' required='true' /]
		[@ww.textfield labelKey='maven.artifact.download.manual.packaging' name='packaging' required='true' /]
		[@ww.textfield labelKey='maven.artifact.download.manual.classifier' name='classifier' required='false' /]
	[/@ui.bambooSection]
	
[/@ui.bambooSection]

[@ui.bambooSection titleKey='maven.artifact.download.maven.config.title']

	[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='mvn3'/][/#assign]
	[@ww.select cssClass="builderSelectWidget" labelKey='executable.type' name='label'
	            list=uiConfigSupport.getExecutableLabels('mvn3')
	            extraUtility=addExecutableLink /]
	
	[#assign addJdkLink][@ui.displayAddJdkInline /][/#assign]
	[@ww.select cssClass="jdkSelectWidget"
	            labelKey='builder.common.jdk' name='buildJdk'
	            list=uiConfigSupport.jdkLabels required='true'
	            extraUtility=addJdkLink /]
	
[/@ui.bambooSection]

[@ui.bambooSection titleKey='maven.artifact.download.maven.config.advanced.title' collapsible=true isCollapsed=!(mavenAdditionalArguments?has_content || environmentVariables?has_content)]
	[@ww.textfield labelKey='maven.artifact.download.maven.additionalArgs' name='mavenAdditionalArguments' cssClass="long-field" /]
	[@ww.textfield labelKey='builder.maven.env' name='environmentVariables' cssClass="long-field" /]
[/@ui.bambooSection]