/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.maven.download;

import static org.gaptap.bamboo.maven.download.Maven3ArtifactDownloadTaskConfigurator.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.davidehringer.bamboo.maven.extractor.InvalidPomException;
import com.davidehringer.bamboo.maven.extractor.PomValueExtractor;
import com.davidehringer.bamboo.maven.extractor.PomValueExtractorMavenModel;

/**
 * Generates the command line for a Maven "download artifact goal."
 * Implementation wise it uses the copy goal on the maven-dependency-plugin.
 * 
 * @author David Ehringer
 */
class MvnDownloadGoal {

    private static final String SEPARATOR = ":";

    private static final String COPY_PLUGIN_GROUPID = "org.apache.maven.plugins";
    private static final String COPY_PLUGIN_ARTIFACTID = "maven-dependency-plugin";
    private static final String COPY_PLUGIN_VERSION = "2.8";
    private static final String COPY_PLUGIN_GOAL = "copy";

    private final CommonTaskContext taskContext;
    private final ConfigurationMap configurationMap;
    private final PomValueExtractor extractor;

    public MvnDownloadGoal(CommonTaskContext taskContext) {
        this.taskContext = taskContext;
        this.configurationMap = taskContext.getConfigurationMap();
        this.extractor = null;
    }

    public MvnDownloadGoal(CommonTaskContext taskContext, PomValueExtractor extractor) {
        this.taskContext = taskContext;
        this.configurationMap = taskContext.getConfigurationMap();
        this.extractor = extractor;
    }

    public static MvnDownloadGoal from(CommonTaskContext taskContext) throws FileNotFoundException, InvalidPomException {
        if (DOWNLOAD_STRATEGY_MANUAL.equals(taskContext.getConfigurationMap().get(DOWNLOAD_STRATEGY))) {
            return new MvnDownloadGoal(taskContext);
        }
        String pom = taskContext.getConfigurationMap().get(POM_FILE);
        File workingDirectory = taskContext.getWorkingDirectory();
        PomValueExtractor extractor = new PomValueExtractorMavenModel(new File(workingDirectory, pom));
        return new MvnDownloadGoal(taskContext, extractor);
    }

    public String getCommandLine() {
        StringBuilder command = new StringBuilder();
        command.append(COPY_PLUGIN_GROUPID);
        command.append(SEPARATOR);
        command.append(COPY_PLUGIN_ARTIFACTID);
        command.append(SEPARATOR);
        command.append(COPY_PLUGIN_VERSION);
        command.append(SEPARATOR);
        command.append(COPY_PLUGIN_GOAL);
        command.append(" -Dartifact=");
        command.append(getGroupId());
        command.append(SEPARATOR);
        command.append(getArtifactId());
        command.append(SEPARATOR);
        command.append(getVersion());
        command.append(SEPARATOR);
        command.append(getPackaging());
        if (useClassifier()) {
            command.append(SEPARATOR);
            command.append(getClassifier());
        }

        command.append(" -DoutputDirectory=");
        command.append(getOutputDirectory());

        command.append(" ");
        command.append(getAdditionalArgs());

        return command.toString();
    }

    private boolean isManual() {
        return DOWNLOAD_STRATEGY_MANUAL.equals(configurationMap.get(DOWNLOAD_STRATEGY));
    }

    private String getGroupId() {
        if (isManual()) {
            return configurationMap.get(GROUP_ID);
        }
        return extractor.getValue("groupId");
    }

    private String getArtifactId() {
        if (isManual()) {
            return configurationMap.get(ARTIFACT_ID);
        }
        return extractor.getValue("artifactId");
    }

    private String getVersion() {
        if (isManual()) {
            return configurationMap.get(VERSION);
        }
        return extractor.getValue("version");
    }

    private Object getPackaging() {
        if (isManual()) {
            return configurationMap.get(PACKAGING);
        }
        String packaging = extractor.getValue("packaging");
        if (packaging == null) {
            packaging = "jar";
        }
        return packaging;
    }

    private boolean useClassifier() {
        if (isManual()) {
            return !StringUtils.isEmpty(configurationMap.get(CLASSIFIER));
        }
        return !StringUtils.isEmpty(configurationMap.get(POM_CLASSIFIER));
    }

    private Object getClassifier() {
        if (isManual()) {
            return configurationMap.get(CLASSIFIER);
        }
        return configurationMap.get(POM_CLASSIFIER);
    }

    private String getOutputDirectory() {
        String directory = configurationMap.get(DOWNLOAD_DESTINATION);
        try {
            if (StringUtils.isEmpty(directory)) {
                return taskContext.getWorkingDirectory().getCanonicalPath();
            }
            return new File(taskContext.getWorkingDirectory(), directory).getCanonicalPath();
        } catch (IOException e) {
            throw new IllegalArgumentException("Unable to use destination download directory.", e);
        }
    }

    private String getAdditionalArgs() {
        return configurationMap.get(MAVEN_ADDITIONAL_ARGS);
    }
}
