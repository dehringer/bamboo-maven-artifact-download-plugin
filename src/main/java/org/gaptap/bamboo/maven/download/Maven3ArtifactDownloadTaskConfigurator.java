/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.maven.download;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.maven.task.AbstractMavenConfig;
import com.atlassian.bamboo.plugins.maven.task.configuration.Maven3BuildTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class Maven3ArtifactDownloadTaskConfigurator extends Maven3BuildTaskConfigurator {

    public static final String DOWNLOAD_DESTINATION = "destination";

    public static final String DOWNLOAD_STRATEGY = "downloadStrategy";
    public static final String DOWNLOAD_STRATEGY_OPTIONS = "downloadStrategies";
    public static final String DOWNLOAD_STRATEGY_POM = "0";
    public static final String DOWNLOAD_STRATEGY_MANUAL = "1";
    public static final String DOWNLOAD_STRATEGY_DEFAULT = "0";

    public static final String POM_FILE = "pomFile";
    public static final String POM_FILE_DEFAULT = "pom.xml";
    public static final String POM_CLASSIFIER = "pomClassifier";

    public static final String GROUP_ID = "groupId";
    public static final String ARTIFACT_ID = "artifactId";
    public static final String VERSION = "version";
    public static final String PACKAGING = "packaging";
    public static final String CLASSIFIER = "classifier";

    public static final String MAVEN_ADDITIONAL_ARGS = "mavenAdditionalArguments";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(DOWNLOAD_DESTINATION, DOWNLOAD_STRATEGY,
            POM_FILE, POM_CLASSIFIER, GROUP_ID, ARTIFACT_ID, VERSION, PACKAGING, CLASSIFIER, MAVEN_ADDITIONAL_ARGS);

    protected TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(DOWNLOAD_STRATEGY, DOWNLOAD_STRATEGY_DEFAULT);
        context.put(POM_FILE, POM_FILE_DEFAULT);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        populateContextForAll(context);
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
        populateContextForAll(context);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> proxyOptions = Maps.newHashMap();
        proxyOptions.put(DOWNLOAD_STRATEGY_POM, textProvider.getText("maven.artifact.download.strategy.pom"));
        proxyOptions.put(DOWNLOAD_STRATEGY_MANUAL, textProvider.getText("maven.artifact.download.strategy.manual"));
        context.put(DOWNLOAD_STRATEGY_OPTIONS, proxyOptions);
    }

    @Override
    public void validate(ActionParametersMap params, ErrorCollection errorCollection) {
        // Bamboo looks for test results by default but we won't have any
        params.put(TaskConfigConstants.CFG_HAS_TESTS, "false");
        // Specify a placeholder value for goals so that the standard Maven
        // validation passes.
        // This will need to be replace in the Task itself with the actual goals
        // to execute
        params.put(AbstractMavenConfig.CFG_GOALS, "-version");
        super.validate(params, errorCollection);

        if (DOWNLOAD_STRATEGY_POM.equals(params.getString(DOWNLOAD_STRATEGY))) {
            assertRequired(params, errorCollection, POM_FILE);
        } else if (DOWNLOAD_STRATEGY_MANUAL.equals(params.getString(DOWNLOAD_STRATEGY))) {
            assertRequired(params, errorCollection, GROUP_ID, ARTIFACT_ID, VERSION, PACKAGING);
        }
    }

    private void assertRequired(ActionParametersMap params, ErrorCollection errorCollection, String... fields) {
        for (String field : fields) {
            if (StringUtils.isEmpty(params.getString(field))) {
                errorCollection.addError(field, textProvider.getText("maven.artifact.download.required.field"));
            }
        }
    }

    // Need to use setting injection here because there are super classes that
    // use setter injection that won't get the proper dependencies injected if
    // we use constructor injection in this subclass
    public void setTextProvider(TextProvider textProvider) {
        this.textProvider = textProvider;
    }

}
