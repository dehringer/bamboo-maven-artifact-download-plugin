/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.maven.download;

import java.io.FileNotFoundException;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.plugins.maven.task.Maven3BuildTask;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.davidehringer.bamboo.maven.extractor.InvalidPomException;

/**
 * A {@link TaskType} for downloading artifacts from Maven repositories.
 * 
 * @author David Ehringer
 */
public class Maven3ArtifactDownloadTask extends Maven3BuildTask {

    public Maven3ArtifactDownloadTask(CapabilityContext capabilityContext,
            EnvironmentVariableAccessor environmentVariableAccessor, ProcessService processService,
            TestCollationService testCollationService) {
        super(capabilityContext, environmentVariableAccessor, processService, testCollationService);
    }

    @Override
    public TaskResult execute(CommonTaskContext originalTaskContext) throws TaskException {
        BuildLogger logger = originalTaskContext.getBuildLogger();

        CommonTaskContext taskContext = null;
        try {
            taskContext = new TaskContextDecorator(originalTaskContext);
        } catch (FileNotFoundException e) {
            logger.addErrorLogEntry("Unabled to find POM file.", e);
            return TaskResultBuilder.newBuilder(originalTaskContext).failedWithError().build();
        } catch (InvalidPomException e) {
            logger.addErrorLogEntry("Invalid POM file.", e);
            return TaskResultBuilder.newBuilder(originalTaskContext).failedWithError().build();
        }

        return super.execute(taskContext);
    }
}
